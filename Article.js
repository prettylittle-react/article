import './Article.scss';

const Article = props => {
    return <div>{props.children}</div>;
};

export default Article;
